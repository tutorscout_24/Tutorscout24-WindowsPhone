﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Security.Credentials;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using TutorScout24.Logic;
using TutorScout24.Logic.UserManagement;

namespace TutorScout24
{
    //Screen for login, also first screen a new user sees when he starts the app
    public sealed partial class LoginScreen : Page
    {
        Backend backend = new Backend();
        public LoginScreen()
        {
            this.InitializeComponent();
        }

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            string mail = mailBox.Text;
            string pw = pwBox.Password;
            performLogin(mail, pw);
        }

        
        private void performLogin(String mail, String pw)
        {
            if (verifyLogin(mail, pw))
            {
                //add login-info to PW-Vault
                CurrentUser.username = mail;
                LoginManager.saveCurrentUser(mail, pw);
                //navigate to mainscreen
                this.Frame.Navigate(typeof(MainScreen), null);
            }
            else //login-info not valid
            {
                errorBlock.Text = backend.returnContent;
            }
        }


        private bool verifyLogin(String mail, String pw)
        {
            //send login-info to backend for verification
            dynamic userJson = new JObject();
            dynamic authentificationJson = new JObject();
            authentificationJson.userName = mail;
            authentificationJson.password = pw;
            userJson.authentication = authentificationJson;
            
            return AsyncHelpers.RunSync<bool>(() => backend.Post("/user/checkAuthentication", userJson.ToString()));
            

        }

        private void registerBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RegisterScreen), null);
        }

        private void pwBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                loginBtn_Click(sender, e);
            }
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //Auto login
            var lastLogin = LoginManager.getLastUserCredentials();
            if (lastLogin != null)
            {
                Debug.Write("DEBUG: auto login\n");
                lastLogin.RetrievePassword();
                performLogin(lastLogin.UserName, lastLogin.Password);
            }
        }
    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic;
using TutorScout24.Logic.Chat;
using TutorScout24.Logic.UserManagement;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Screens
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ChatScreen : Page
    {
        string user;
        MessagePool messagePool;
        List<IMessage> messages = new List<IMessage>();
        Backend backend = new Backend();
        List<IMessage> shownMessages = new List<IMessage>();
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            user = (e.Parameter as ChatScreenArgs).username;
            messagePool = (e.Parameter as ChatScreenArgs).messagePool;
        }

        public ChatScreen()
        {
            this.InitializeComponent();
        }
        private void back(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            UserName.Text = user;
            await messagePool.loadAsync();
            loadChat();
            scrollDown();
        }
        private async void loadChat()
        {
            await messagePool.loadAsync();

            shownMessages = messagePool.getChatWith(user);
            ChatListView.ItemsSource = shownMessages;
            scrollDown();

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            dynamic sendJson = new JObject();
            sendJson.toUserId = user;
            sendJson.text = sendTextBox.Text;
            sendJson.authentication = CurrentUser.getAuthenticationJson();
            if(await backend.Post("/message/sendMessage", sendJson.ToString()))
            {
                sendTextBox.Text = "";
                loadChat();
            }

        }
        private void scrollDown()
        {
            var selectedIndex = ChatListView.Items.Count - 1;
            if (selectedIndex < 0)
                return;

            ChatListView.SelectedIndex = selectedIndex;
            ChatListView.UpdateLayout();

            ChatListView.ScrollIntoView(ChatListView.SelectedItem);
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            loadChat();
            scrollDown();
        }

        private void sendTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if(e.Key == Windows.System.VirtualKey.Enter)
            {
                Button_Click(new object(), new RoutedEventArgs());
            }
        }
        private void Grid_Holding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            // If you need the clicked element:
            // Item whichOne = senderElement.DataContext as Item;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }
    }
}

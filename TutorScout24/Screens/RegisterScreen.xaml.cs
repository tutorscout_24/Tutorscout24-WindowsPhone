﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using Newtonsoft.Json.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TutorScout24.Logic;


namespace TutorScout24
{
    //screen whitch contains a form to fill out for creating a new user
    public sealed partial class RegisterScreen : Page
    {
        public RegisterScreen()
        {
            this.InitializeComponent();
        }

        //check basic constraints locally to avoid unneccessary waiting
        private bool localValidation()
        {
            if(Username.Text.Length < 3)
            {
                Error.Text = "Benutzername muss mindestens 3 Zeichen lang sein!";
                return false;
            }
            if(Firstname.Text.Length < 2)
            {
                Error.Text = "Vorname muss mindestens 2 Zeichen lang sein!";
                return false;
            }
            if(Lastname.Text.Length < 2)
            {
                Error.Text = "Nachname muss mindestens 2 Zeichen lang sein!";
                return false;
            }
            if(Birthdate.Text.Length != 8)
            {
                Error.Text = "Geburtsdatum muss im Format JJJJMMTT angegeben werden!";
                return false;
            }
            if(Gender.Text.Length < 2)
            {
                Error.Text = "Geschlecht muss mindestens 4 Zeichen lang sein!";
                return false;
            }
            if(Email.Text.Length < 1)
            {
                Error.Text = "Bitte Email angeben!";
                return false;
            }
            if(Password.Password.Length < 8)
            {
                Error.Text = "Passwort muss mindestens 8 Zeichen lang sein, Groß-/Kleinbuchstaben, Zahlen und Sonderzeichen enthalten!";
                return false;
            }
            if(Password.Password != PasswordConfirm.Password)
            {
                Error.Text = "Die beiden Passwörter stimmen nicht überein!";
                return false;
            }
            return true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(LoginScreen));
        }

        private async void Register_Click(object sender, RoutedEventArgs e)
        {
            if (localValidation())
            {
                //build json object for backend
                dynamic userJason = new JObject();
                userJason.userName = Username.Text;
                userJason.password = Password.Password;
                userJason.firstName = Firstname.Text;
                userJason.lastName = Lastname.Text;
                userJason.birthdate = Birthdate.Text;
                userJason.gender = Gender.Text;
                userJason.email = Email.Text;
                userJason.note = Note.Text;
                userJason.placeOfResidence = PlaceOfResident.Text;
                userJason.maxGraduation = maxGraduation.Text;
                //send json object to backend
                Backend backend = new Backend();
                await backend.Post("/user/create", userJason.ToString());
                //process backend response
                if (backend.returnContent.Contains("[instance.email]"))
                {
                    Error.Text = "Ungültige Email!";
                }
                else if (backend.returnContent.Contains("[instance.password]"))
                {
                    Error.Text = "Passwort muss mindestens 8 Zeichen lang sein, Groß-/Kleinbuchstaben, Zahlen und Sonderzeichen enthalten!";
                }
                else
                {
                    Error.Text = backend.returnContent;
                }
            }
        }
    }
}

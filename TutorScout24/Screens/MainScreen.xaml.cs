﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic.UserManagement;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace TutorScout24
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainScreen : Page
    {
        public MainScreen()
        {
            this.InitializeComponent();
        }

        private void ProfilePage_LogOutEvent(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(LoginScreen), null);
        }

        private void PivotBar_Loaded(object sender, RoutedEventArgs e)
        {
            setPivotBarColor();
        }

        private void ProfilePage_ToggleAppModeEvent(Pages.ProfilePage proPage, EventArgs e)
        {
            setPivotBarColor();
        }

        private void setPivotBarColor()
        {
            if (CurrentUser.type == Logic.AdvertManagement.CreatorType.Tutor)
            {
                PivotBar.Background = CurrentUser.TUTORCOLOR;
            }
            if (CurrentUser.type == Logic.AdvertManagement.CreatorType.Student)
            {

                PivotBar.Background = CurrentUser.STUDENTCOLOR;
            }
        }
    }
}

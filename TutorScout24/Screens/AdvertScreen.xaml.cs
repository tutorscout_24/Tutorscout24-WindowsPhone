﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TutorScout24.Logic.UserManagement;
using TutorScout24.Logic.AdvertManagement;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Screens
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AdvertScreen : Page
    {
        Advert advert;
        public AdvertScreen()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            advert = e.Parameter as Advert;
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (advert.isAccepted)
            {
                CurrentUser.acceptedAdverts.Remove(advert);
                (sender as AppBarButton).Icon = new SymbolIcon(Symbol.Add);

            }
            else
            {
                CurrentUser.acceptedAdverts.Add(advert);
                (sender as AppBarButton).Icon = new SymbolIcon(Symbol.Accept);
            }
        }

        private void back(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            title.Text = advert.Title;
            autor.Text = advert.Autor;
            date.Text = advert.Date.ToString();
            text.Text = advert.Description;
            if (advert.isAccepted)
            {
                AddButton.Icon = new SymbolIcon(Symbol.Accept);
            }
            this.Background = advert.color;
        }

        private void StartChat(object sender, RoutedEventArgs e)
        {
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(ChatScreen), new ChatScreenArgs(advert.Autor, new Logic.Chat.MessagePool()));
        }
    }
}

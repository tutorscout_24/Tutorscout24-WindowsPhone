﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic;
using TutorScout24.Logic.UserManagement;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Credentials;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Pages
{

    public sealed partial class EditProfileScreen : Page
    {
        public EditProfileScreen()
        {
            this.InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //set textbox values from current user
            UsernameTB.Text = CurrentUser.username;
            FirstNameTB.Text = CurrentUser.firstName;
            LastNameTB.Text = CurrentUser.lastname;
            GenderTB.Text = CurrentUser.gender;
            GraduationTB.Text = CurrentUser.maxGraduation;
            DescriptionTB.Text = CurrentUser.description;
            MailTB.Text = CurrentUser.email;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainScreen), null);
        }

        private async void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            PasswordCredential credential = LoginManager.getLastUserCredentials();
            credential.RetrievePassword();
            //build json object
            dynamic userData = new JObject();
            userData.password = credential.Password;
            userData.firstName = FirstNameTB.Text;
            userData.lastName = LastNameTB.Text;
            userData.gender = GenderTB.Text;
            if (MailTB.Text != CurrentUser.email)
            {
                userData.email = MailTB.Text;
            }
            userData.note = DescriptionTB.Text;
            userData.maxGraduation = GraduationTB.Text;
            userData.authentication = CurrentUser.getAuthenticationJson();
            //send json object to backend
            Backend backend = new Backend();
            await backend.Put("/user/updateUser", userData.ToString());
            Debug.WriteLine("DEBUG: EditProfile: Backend Code: " + backend.code);
            Debug.WriteLine("DEBUG: EditProfile: Backend answer: " + backend.returnContent);

            this.Frame.Navigate(typeof(MainScreen), null);
        }
    }
}

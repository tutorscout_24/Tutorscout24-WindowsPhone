﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.Chat;

namespace TutorScout24.Screens
{
    class ChatScreenArgs
    {
        public string username;
        public MessagePool messagePool;
        public ChatScreenArgs(string username, MessagePool messagePool)
        {
            this.username = username;
            this.messagePool = messagePool;
        }
    }
}

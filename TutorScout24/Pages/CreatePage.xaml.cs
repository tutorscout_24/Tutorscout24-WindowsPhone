﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TutorScout24.Logic.AdvertManagement;
using TutorScout24.Logic.AdvertManagement.AdvertCreation;
using TutorScout24.Logic.UserManagement;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreatePage : Windows.UI.Xaml.Controls.Page
    {
        public CreatePage()
        {
            this.InitializeComponent();
        }

        private async void CreateBtn_Click(object sender, RoutedEventArgs e)
        {
            AdvertCommiter advertCommiter = new AdvertCommiter();
            Advert advert = await AdvertCreator.CreateAdvertAsync(TitleTextBox.Text, DiscriptionTextBox.Text);
            if (advert == null)
            {
                CurrentUser.PromptMissingAccessAsync();
                return;
            }
            advertCommiter.Commit(advert);
        }
    }
}

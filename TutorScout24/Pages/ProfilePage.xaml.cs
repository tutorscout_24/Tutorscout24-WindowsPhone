﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic.UserManagement;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Page that shows the profile information of the current user
namespace TutorScout24.Pages
{
    public sealed partial class ProfilePage : Page
    {
        public event EventHandler LogOutEvent;
        public event EventHandler ToggleAppModeEvent;
        public delegate void EventHandler(ProfilePage proPage, EventArgs e);

        public ProfilePage()
        {
            this.InitializeComponent();
        }
        


        private void ModeSwitchBtn_Click(object sender, RoutedEventArgs e)
        {
            //switch between tutor-mode and student-mode
            CurrentUser.ToggleCreatorType();
            ToggleAppModeEvent(this, null);
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(MainScreen), null);
        }

        private void LogOutBtn_Click(object sender, RoutedEventArgs e)
        {
            //emit logout event and navigate to LoginScreen
            Logic.UserManagement.LoginManager.removeCurrentUser();
            LogOutEvent(this, null);
        }

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            await CurrentUser.getCurrentUserDataFromBackend();
            try
            {
                //set textbox values from current user
                UsernameLbl.Text = CurrentUser.username;
                FirstNameLbl.Text = CurrentUser.firstName;
                LastNameLbl.Text = CurrentUser.lastname;
                GenderLbl.Text = CurrentUser.gender;
                GraduationLbl.Text = CurrentUser.maxGraduation;
                DescriptionLbl.Text = CurrentUser.description;
                MailLbl.Text = CurrentUser.email;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("DEBUG: Profile: missing values");
            }
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            //navigate to EditProfileScreen
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(EditProfileScreen), null);
        }
    }
}

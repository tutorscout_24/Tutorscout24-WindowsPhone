﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic.Chat;
using TutorScout24.Screens;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MessagesPage : Page
    {
        //BackgroundWorker ChatLoader = new BackgroundWorker();
        Deferral deferral;
        MessagePool messagePool = new MessagePool();
        List<Chat> chats;

        public MessagesPage()
        {
            this.InitializeComponent();
            //var runCompletedHandler = new RunWorkerCompletedEventHandler((object o, RunWorkerCompletedEventArgs er) => { if (deferral != null) deferral.Complete(); deferral = null; });
            //ChatLoader.RunWorkerCompleted += runCompletedHandler;
            //ChatLoader.DoWork += new DoWorkEventHandler(loadChats);
            //ChatLoader.RunWorkerCompleted += new RunWorkerCompletedEventHandler(showChats);

        }

        private void showChats(/*object sender, RunWorkerCompletedEventArgs e*/)
        {
            TheOneRing.IsActive = false;
            ChatListView.ItemsSource = chats;
        }

        private async void loadChats(/*object sender, DoWorkEventArgs e*/)
        {
            await messagePool.loadAsync();
            chats = messagePool.getChats();
            if (deferral != null) deferral.Complete(); deferral = null;
            showChats();
        }

        private void AdvertListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Chat chat = e.ClickedItem as Chat;
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(ChatScreen), new ChatScreenArgs(chat.userName, messagePool));
        }

        private void PullToRefreshExtender_RefreshRequested(object sender, AmazingPullToRefresh.Controls.RefreshRequestedEventArgs e)
        {
            deferral = e.GetDeferral();
            loadChats();
            //ChatLoader.RunWorkerAsync();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TheOneRing.IsActive = true;
            loadChats();
            //ChatLoader.RunWorkerAsync();
        }
    }
}

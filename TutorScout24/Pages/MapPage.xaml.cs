﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TutorScout24.Logic.AdvertManagement;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.Storage.Streams;
using TutorScout24.Logic.UserManagement;
using TutorScout24.Logic.AdvertManagement.AdvertSearch;
using System.ComponentModel;
using TutorScout24.Screens;


namespace TutorScout24.Pages
{
    //Page to show a Map with the available adverts
    public sealed partial class MapPage : Page
    {
        //dictionary to associate a marker with its corresponding advert
        Dictionary<MapElement, Advert> advertDict = new Dictionary<MapElement, Advert>();

        List<Advert> pool = new List<Advert>();

        public MapPage()
        {
            this.InitializeComponent();
            initAdvertManagement();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Map.MapServiceToken = "JT3i4BP1LHwN46qUe7aL~nVYEhG0AIIORwL2qHcnCKQ~ApQ9I6bdXVhXNiA_Hop-sKCEFjV_2I7ok5e5SFV_BLhp7hP7PsGpmnTlfeBMMNKv";

            Map.Center = new Geopoint(new BasicGeoposition() { Latitude = 50, Longitude = 10 });
            Map.ZoomLevel = 12;
            Map.LandmarksVisible = true;

            try
            {
                //Set the map location to current user position.
                Geoposition pos = AsyncHelpers.RunSync<Geoposition>(() => CurrentUser.getCurrentPositionAsync());
                Geopoint myLocation = pos.Coordinate.Point;
                Map.Center = myLocation;
                Map.ZoomLevel = 12;
            }
            catch (Exception)
            {

            }
        }

        //add a new marker for a single advert to the map
        public void AddAdvertAsMarker(Advert advert)
        {
            //create marker from advert
            BasicGeoposition position = advert.Position;
            Geopoint mapPoint = new Geopoint(position);
            MapIcon markerIcon = new MapIcon
            {
                Location = mapPoint,
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                ZIndex = 0,
                Title = advert.Title,
                Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/map-icon.png"))
                
            };
            //add marker
            advertDict.Add(markerIcon, advert);
            Debug.WriteLine("DEBUG: Map: added advert: " + advert.Title);

            updateMap();
        }

        public void updateMap()
        {
            //add all markers stored in advertDict to the map
            foreach (KeyValuePair<MapElement, Advert> entry in advertDict)
            {
                Map.MapElements.Add(entry.Key);
            }
        }

        //navigate to AdvertScreen when a marker is clicked
        private void Map_MapElementClick(MapControl sender, MapElementClickEventArgs args)
        {
            //https://stackoverflow.com/questions/34377203/how-to-create-mapicon-event-in-uwp
            MapElement ClickedIcon = args.MapElements.FirstOrDefault(x => x is MapIcon) as MapIcon;

            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(AdvertScreen), advertDict[ClickedIcon]);
        }



        private void initAdvertManagement()
        {
            Debug.WriteLine("DEBUG: Map: entered initAdvertStuff()");
            loadAdvertsFromBackend();
            processAdvertsFromPool();
             
        }

        public void loadAdvertsFromBackend()
        {
            Debug.WriteLine("DEBUG: Map: entered loadAdvertsFromBackend()");
            AdvertFactory factory = new AdvertFactory();
            AdvertRequest request = new AdvertRequest(1000, 0);
            pool = factory.CreatePool(request);

        }

        //call AddAdvertAsMarker() for every available advert
        private void processAdvertsFromPool()
        {
            Debug.WriteLine("DEBUG: Map: entered processAdvertsFromPool()");
            foreach (Advert ad in pool)
            {
                AddAdvertAsMarker(ad);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TutorScout24.Logic.AdvertManagement;
using TutorScout24.Logic.UserManagement;
using TutorScout24.Screens;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TutorScout24.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TutorienPage : Page
    {
        public TutorienPage()
        {
            this.InitializeComponent();
        }

        private void AdvertListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Advert advert = e.ClickedItem as Advert;
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(AdvertScreen), advert);
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
            AdvertListView.ItemsSource = CurrentUser.acceptedAdverts;
        }
    }
}

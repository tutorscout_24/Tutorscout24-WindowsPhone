﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using TutorScout24.Screens;
using TutorScout24.Logic.AdvertManagement;
using TutorScout24.Logic.AdvertManagement.AdvertSearch;
using System.ComponentModel;
using AmazingPullToRefresh.Controls;

namespace TutorScout24.Pages
{

    public sealed partial class SearchPage 
    {
        BackgroundWorker AdvertLoader = new BackgroundWorker();
        Deferral deferral;
        List<Advert> pool = new List<Advert>();
        public SearchPage()
        {
            this.InitializeComponent();
            AdvertLoader.DoWork += new DoWorkEventHandler(loadAdvertsFromBackend);
            AdvertLoader.RunWorkerCompleted += new RunWorkerCompletedEventHandler(showAdverts);
            var runCompletedHandler = new RunWorkerCompletedEventHandler((object o, RunWorkerCompletedEventArgs er) => { if (deferral != null) deferral.Complete(); deferral = null; });
            AdvertLoader.RunWorkerCompleted += runCompletedHandler;
        }

        public void loadAdvertsFromBackend(object sender, DoWorkEventArgs e)
        {
            AdvertFactory factory = new AdvertFactory();
            AdvertRequest request = new AdvertRequest(100,0);
            pool = factory.CreatePool(request);     
            
        }
        public void showAdverts(object sender, RunWorkerCompletedEventArgs e)
        {
            
            TheOneRing.IsActive = false;
            AdvertListView.ItemsSource = pool;


        }

        
        private async System.Threading.Tasks.Task sendGetAsync(String suffix)
        {
            string responseBody = "";
            try
            {
                Windows.Web.Http.HttpClient client = new Windows.Web.Http.HttpClient();

                Windows.Web.Http.HttpResponseMessage response = new Windows.Web.Http.HttpResponseMessage();

                Uri requestUri = new Uri("http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1" + suffix);
                Debug.Write("DEBUG: requestUri: " + requestUri + "\n");
                response = await client.GetAsync(requestUri);
                responseBody = await response.Content.ReadAsStringAsync();

                Debug.Write("DEBUG: response: " + response +"\n");
                Debug.Write("Debug: body: " + responseBody + "\n");

                dynamic responseJson = JObject.Parse(responseBody);

                string userCount = responseJson.userCount;
                Debug.Write("DEBUG: userCount: " + userCount + "\n");
            }
            catch (Exception ex)
            {
                Debug.Write("DEBUG: catch ex\n");
            }
            
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
            TheOneRing.IsActive = true;
            AdvertLoader.RunWorkerAsync();
        }

        private void AdvertListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Advert advert = e.ClickedItem as Advert;
            var frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(AdvertScreen), advert);
        }
        private void PullToRefreshExtender_RefreshRequested(object sender, RefreshRequestedEventArgs e)
        {
            deferral = e.GetDeferral();
            AdvertLoader.RunWorkerAsync();
        }

        private void Grid_GettingFocus(UIElement sender, GettingFocusEventArgs args)
        {
            Debug.WriteLine("YAY it works");
        }
    }
}

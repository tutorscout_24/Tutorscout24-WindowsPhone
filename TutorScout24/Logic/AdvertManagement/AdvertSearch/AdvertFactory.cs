﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TutorScout24.Logic.UserManagement;
using Windows.Devices.Geolocation;

namespace TutorScout24.Logic.AdvertManagement.AdvertSearch
{
    class AdvertFactory
    {
        Backend backend = new Backend();
        /**
         * returns a list with all to the request matching Adverts from the Backend 
         */

        public List<Advert> CreatePool(AdvertRequest request)
        {
            string sufix;
            switch (CurrentUser.type)
            {
                case CreatorType.Tutor:
                    sufix = "/tutoring/requests";
                    break;
                case CreatorType.Student:
                    sufix = "/tutoring/offers";
                    break;
                default:
                    throw new Exception("No User Type given");
            }
            bool ok = AsyncHelpers.RunSync<bool>(() => backend.Post(sufix, request.getJsonRequest().ToString()));
            if (backend.code == HttpStatusCode.OK)
            {
                string returnString = backend.returnContent;
                JArray JsonArray = JArray.Parse(returnString);
                List<Advert> advertList = new List<Advert>();
                foreach (JToken token in JsonArray)
                {
                    Advert advert = new Advert();
                    advert.Autor = token.Value<string>("userName");
                    advert.Description = token.Value<string>("text");
                    advert.Date = DateTime.Parse(token.Value<string>("creationDate"));
                    advert.Title = token.Value<string>("subject");
                    switch (sufix)
                    {
                        case "/tutoring/requests":
                            advert.CreatorType = CreatorType.Student;
                            break;
                        case "/tutoring/offers":
                            advert.CreatorType = CreatorType.Tutor;
                            break;
                    }
                    advertList.Add(advert);
                    BasicGeoposition position = new BasicGeoposition();
                    position.Latitude = token.Value<double>("latitude");
                    position.Longitude = token.Value<double>("longitude");
                    advert.Position = position;
                }
                return advertList;
            }
            return new List<Advert>();
            //throw new Exception(backend.returnContent);
        }
    }
}

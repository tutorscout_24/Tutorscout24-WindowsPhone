﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorScout24.Logic.AdvertManagement.AdvertSearch
{
    class Location
    {
        public Location()
        {
            latitude = "100";
            longitude = "100";
            rangeKm = "10000";
        }
        public string latitude { set; get; }
        public string longitude { set; get; }
        public string rangeKm { set; get; }
    }
}

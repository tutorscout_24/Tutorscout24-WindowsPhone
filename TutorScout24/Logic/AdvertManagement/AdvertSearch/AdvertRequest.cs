﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.Devices.Geolocation;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TutorScout24.Logic.UserManagement;

namespace TutorScout24.Logic.AdvertManagement.AdvertSearch
{
    class AdvertRequest
    {
        private int maxRow;
        private int rowOffset;
        public AdvertRequest(int maxRow, int rowOffset)
        {
            this.maxRow = maxRow;
            this.rowOffset = rowOffset;
        }
        /**
         * Creates a Json Object that contains all needed informations for the Backend.
         */

        public JObject getJsonRequest()
        {
            dynamic Jrequest = new JObject();
            Geoposition postition;
            try
            {
                postition = AsyncHelpers.RunSync<Geoposition>(() => CurrentUser.getCurrentPositionAsync());
                Jrequest.latitude = postition.Coordinate.Point.Position.Latitude;
                Jrequest.longitude = postition.Coordinate.Point.Position.Longitude;
            }
            catch(Exception e)
            {
                Jrequest.latitude = 10;
                Jrequest.longitude = 10;
            }

            Jrequest.rangeKm = CurrentUser.searchRange;
            Jrequest.rowLimit = maxRow;
            Jrequest.rowOffset = rowOffset;
            Jrequest.authentication = CurrentUser.getAuthenticationJson();
            return Jrequest;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TutorScout24.Logic.UserManagement;

namespace TutorScout24.Logic.AdvertManagement.AdvertCreation
{
    class AdvertCommiter
    {
        Backend backend = new Backend();
        /**
         * Commits the avdert in the parameter to the Backend.
         */

        public void Commit(Advert advert)
        {
            dynamic advertJason = new JObject();
            advertJason.subject = advert.Title;
            advertJason.text = advert.Description;
            advertJason.duration = 10;
            advertJason.latitude = advert.Position.Latitude;
            advertJason.longitude = advert.Position.Longitude;
            advertJason.authentication = CurrentUser.getAuthenticationJson();
            string suffix = "";
            switch (advert.CreatorType)
            {
                case CreatorType.Tutor:
                    suffix = "/tutoring/createOffer";
                    break;
                case CreatorType.Student:
                    suffix = "/tutoring/createRequest";
                    break;
            }
            if (suffix != "")
            {
                AsyncHelpers.RunSync(() => backend.Post(suffix, advertJason.ToString()));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.UserManagement;
using Windows.Devices.Geolocation;

namespace TutorScout24.Logic.AdvertManagement.AdvertCreation
{
    static class AdvertCreator
    {
        /**
         * Creates An Advert with the Title and Description from the parameters, 
         * the current position and informations from the current user.
         */

        public static async Task<Advert> CreateAdvertAsync(string title, string discription)
        {
            Advert advert = new Advert();
            advert.Title = title;
            //advert.Tags = tags.Split(' ');
            advert.Description = discription;
            BasicGeoposition pos = new BasicGeoposition();
            try
            {
                Geoposition geoPos = await CurrentUser.getCurrentPositionAsync();
                pos.Latitude = geoPos.Coordinate.Point.Position.Latitude;
                pos.Longitude = geoPos.Coordinate.Point.Position.Longitude;
            }
            catch (Exception e)
            {
                Debug.WriteLine("DEBUG: AdCreator: Could not create Advert");
                return null;
            }
            advert.CreatorType = CurrentUser.type;
            advert.Position = pos;
            advert.Date = DateTime.Now;
            return advert;
        }
    }
}

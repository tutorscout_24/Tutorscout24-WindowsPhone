﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using TutorScout24.Logic.UserManagement;
using Windows.UI.Xaml.Media;
using Windows.UI;

namespace TutorScout24.Logic.AdvertManagement
{
    public enum CreatorType
    {
        Tutor,
        Student
    }
    public class Advert
    {
        public Advert()
        {
            //    Position = new Position();
            Autor = "Mr. DEBUG und sein treuer Freund und Helfer Dr. NotNULL";
            Description = "Mr. Debug wollte nicht, dass eine Description leer ist. Er kannte das problem, denn er wollte schonmal eine Description lesen, doch diese war leer. Darauf hin bekam er einen herzinfakt. Jetzt darf Dr. NotNull alle leeren Descriptions ausfüllen.";
            Date = DateTime.Now;
            Title = "Anzeige.";
        }
        //public Position Position { set; get; }
        public bool isAccepted { get { return CurrentUser.acceptedAdverts.Contains(this); } }
        public BasicGeoposition Position; //testing
        public string[] Tags { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public DateTime Date { set; get; }
        public CreatorType CreatorType { set; get; }
        public string Autor { get; internal set; }
        public SolidColorBrush color { get
            {
                switch (CreatorType)
                {
                    case CreatorType.Student:
                        return new SolidColorBrush(Color.FromArgb(255, 219, 46, 0));
                    case CreatorType.Tutor:
                        return new SolidColorBrush(Color.FromArgb(255, 0, 50, 233));
                }
                return null;
            }
        }
    }
}

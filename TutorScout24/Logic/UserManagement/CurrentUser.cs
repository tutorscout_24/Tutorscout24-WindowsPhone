﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Credentials;
using Newtonsoft.Json.Linq;
using Windows.Devices.Geolocation;
using TutorScout24.Logic.AdvertManagement;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI;

namespace TutorScout24.Logic.UserManagement
{
    //static class that stores the information of the current user
    static class CurrentUser
    {
        //colors
        public static SolidColorBrush STUDENTCOLOR = new SolidColorBrush(Color.FromArgb(255, 48, 179, 221));
        public static SolidColorBrush TUTORCOLOR = new SolidColorBrush(Color.FromArgb(255, 198, 57, 57));
        //location
        private static Boolean accessMissingPrompted = false;
        private static Boolean useLiveLocation = true;
        //adverts
        public static List<Advert> acceptedAdverts = new List<Advert>();
        public static CreatorType type;
        public static int searchRange = 100;
        //general information
        public static string username;
        public static string email;
        public static string firstName;
        public static string lastname;
        public static string gender;
        public static string description;
        public static string maxGraduation;

        //toggle between tutor-mode and student-mode
        public static void ToggleCreatorType()
        {
            if(type == CreatorType.Tutor)
            {
                type = CreatorType.Student;
            } else
            {
                type = CreatorType.Tutor;
            }
        }

        //request information of the current user from backend
        public static async Task getCurrentUserDataFromBackend()
        {
            dynamic userData = new JObject();
            userData.authentication = CurrentUser.getAuthenticationJson();
            Backend backend = new Backend();
            await backend.Post("/user/myUserInfo", userData.ToString());
            Debug.WriteLine("DEBUG: CurrUsr: Backend answer: " + backend.returnContent);
            //set variables with values from backend response
            dynamic responseJson = JObject.Parse(backend.returnContent);
            firstName = responseJson.firstName;
            lastname = responseJson.lastName;
            gender = responseJson.gender;
            description = responseJson.description;
            maxGraduation = responseJson.maxGraduation;
            email = responseJson.email;

        }

        public static JObject getAuthenticationJson()
        {
            dynamic userJson = new JObject();
            userJson.userName = username;
            PasswordCredential credential = LoginManager.getLastUserCredentials();
            credential.RetrievePassword();
            userJson.password = credential.Password;
            return userJson;
        }

        //get the current position of the user
        //check if app has sufficient permissions
        public static async Task<Geoposition> getCurrentPositionAsync()
        {
            if (useLiveLocation)
            {
                var accessStatus = await Geolocator.RequestAccessAsync();

                switch (accessStatus)
                {
                    case GeolocationAccessStatus.Allowed:
                        Debug.WriteLine("DEBUG: access allowed");
                        // Get the current location.
                        Geolocator geolocator = new Geolocator();
                        Geoposition pos = await geolocator.GetGeopositionAsync();
                        return pos;
                    case GeolocationAccessStatus.Denied:
                        Debug.WriteLine("DEBUG: access denied");
                        if (!accessMissingPrompted)
                        {
                            PromptMissingAccessAsync();
                        }
                        break;

                    case GeolocationAccessStatus.Unspecified:
                        Debug.WriteLine("DEBUG: unspecified access");
                        if (!accessMissingPrompted)
                        {
                            PromptMissingAccessAsync();
                        }
                        break;
                }
            }
            return null;
        }

        //display a warning, that the app has insufficient permissions
        public static async Task<ContentDialogResult> PromptMissingAccessAsync()
        {
            ContentDialog missingGeolocationAccess = new ContentDialog
            {
                Title = "Keine Standortberechtigung!",
                Content = "Um TutorScout24 vollständig nutzen zu können müssen sie der App zugriff auf ihren Standort gewähren.",
                CloseButtonText = "Ok"
            };
            accessMissingPrompted = true;
            ContentDialogResult ret = await missingGeolocationAccess.ShowAsync();
            return ret;
        }

    }


}

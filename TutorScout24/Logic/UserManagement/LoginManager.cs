﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Credentials;
using System.Diagnostics;


namespace TutorScout24.Logic.UserManagement
{
    class LoginManager
    {
        const string APPRESSOURCE = "TutorScout24";

        //to be called whenever a new user logs into the app
        public static void saveCurrentUser(string mail, string pw)
        {
            PasswordVault pwVault = new PasswordVault();
            pwVault.Add(new PasswordCredential(APPRESSOURCE, mail, pw));
        }

        //to be called whenever a user logs out of the app
        public static void removeCurrentUser()
        {
            removeUser(CurrentUser.username);
        }

        private static void removeUser(string mail)
        {
            PasswordVault pwVault = new PasswordVault();
            pwVault.Remove(pwVault.Retrieve(APPRESSOURCE, mail));
        }

        public static PasswordCredential getLastUserCredentials()
        {
            PasswordVault pwVault = new PasswordVault();
            try
            {
                var savedCredentials = pwVault.FindAllByResource(APPRESSOURCE);
                if (savedCredentials.Count > 1)
                {
                    removeUser("admin");
                }
                if(savedCredentials.Count == 1)
                {
                    return savedCredentials[0];
                }
            }
            catch(System.Runtime.InteropServices.COMException)
            {
                Debug.Write("DEBUG: catch");
            }
            return null;
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Newtonsoft.Json.Linq;
using HttpClient = Windows.Web.Http.HttpClient;
using HttpResponseMessage = Windows.Web.Http.HttpResponseMessage;
using HttpStatusCode = System.Net.HttpStatusCode;

namespace TutorScout24.Logic
{
    internal class Backend
    {
        private readonly string url = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1";
        public string returnContent;
        public HttpStatusCode code;

        public async Task<bool> Post(string suffix, string jsonContent)
        {
            Uri requestUri = new Uri(url + suffix);
            var httpClient = new System.Net.Http.HttpClient();

            try
            {
                Debug.WriteLine("DEBUG: Backend: Posted:" + jsonContent);
                System.Net.Http.HttpResponseMessage response = await httpClient.PostAsync(requestUri,
                    new StringContent(jsonContent, Encoding.UTF8, "application/json"));
                returnContent = await response.Content.ReadAsStringAsync();
                code = response.StatusCode;
                Debug.WriteLine("DEBUG: Backend: response: " + returnContent);
                return response.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                returnContent = e.Message;
                code = HttpStatusCode.BadRequest;
                return false;
            }

        }

        public async Task<bool> Put(string suffix, string jsonContent)
        {
            Uri requestUri = new Uri(url + suffix);
            var httpClient = new System.Net.Http.HttpClient();

            try
            {
                Debug.WriteLine("DEBUG: Backend: Put:" + jsonContent);

                System.Net.Http.HttpResponseMessage response = await httpClient.PutAsync(requestUri,
                    new StringContent(jsonContent, Encoding.UTF8, "application/json"));
                returnContent = await response.Content.ReadAsStringAsync();
                code = response.StatusCode;
                return response.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                returnContent = e.Message;
                code = HttpStatusCode.BadRequest;
                return false;
            }

        }
    }
}
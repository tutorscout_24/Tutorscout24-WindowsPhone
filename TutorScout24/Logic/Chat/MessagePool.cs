﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.UserManagement;

namespace TutorScout24.Logic.Chat
{
    class MessagePool
    {
        public List<SendMessage> sendMessages = new List<SendMessage>();
        public List<ReseivedMessage> resievedMessages = new List<ReseivedMessage>();
        Backend backend = new Backend();

        /**
         * returns all Messages with the user from the parameter.
         */

        public List<IMessage> getChatWith(string username)
        {
            List<IMessage> messages = new List<IMessage>();
            IEnumerable<SendMessage> sendMessagetoUser = sendMessages.Where(m => m.User == username);
            IEnumerable<ReseivedMessage> reseivedMessageFromUser = resievedMessages.Where(m => m.User == username);
            messages.AddRange(sendMessagetoUser);
            messages.AddRange(reseivedMessageFromUser);
            messages = messages.OrderBy(m => m.time).ToList();
            return messages;
        }
        /**
         * returns all chats the currend user had with other users.
         */

        public List<Chat> getChats()
        {
            List<Chat> chats = new List<Chat>();
            List<IMessage> allMessages = new List<IMessage>();
            allMessages.AddRange(sendMessages);
            allMessages.AddRange(resievedMessages);
            allMessages = allMessages.OrderBy(m => m.time).ToList();
            allMessages.Reverse();

            foreach (IMessage message in allMessages)
            {
                if(!chats.Any(m => m.userName == message.User))
                {
                    bool type = message is SendMessage;

                    int messigesUnread = allMessages.Where(rm => rm.notread).Count();
                    chats.Add(new Chat
                    {
                        userName = message.User,
                        lastMessageText = message.text,
                        lastMessageSend = type,
                        time = message.time,
                        unreadMessages = messigesUnread
                    });
                }
            }
            return chats;
        }
        /**
         * reload the lists of masseges from the Backend
         */
        public async Task loadAsync()
        {
            sendMessages = new List<SendMessage>();
            resievedMessages = new List<ReseivedMessage>();
            dynamic requestJson = new JObject();
            requestJson.authentication = CurrentUser.getAuthenticationJson();
            if(AsyncHelpers.RunSync<bool>(() => backend.Post("/message/getSentMessages", requestJson.ToString())))
            {
                string returnString = backend.returnContent;
                JArray JsonArray = JArray.Parse(returnString);
                foreach (JToken token in JsonArray)
                {
                    SendMessage message = new SendMessage(token.Value<string>("text"));
                    message.User = token.Value<string>("toUserId");
                    message.time = DateTime.Parse(token.Value<string>("datetime"));
                    sendMessages.Add(message);
                }
            }
            else if (!(backend.code == System.Net.HttpStatusCode.Conflict))
            {
                throw new Exception(backend.code.ToString());
            }
            if (await backend.Post("/message/getReceivedMessages", requestJson.ToString()))
            {
                string returnString = backend.returnContent;
                JArray JsonArray = JArray.Parse(returnString);
                AdvertManagement.CreatorType type = AdvertManagement.CreatorType.Student;
                switch (CurrentUser.type)
                {
                    case AdvertManagement.CreatorType.Student:
                        type = AdvertManagement.CreatorType.Tutor;
                        break;
                    case AdvertManagement.CreatorType.Tutor:
                        type = AdvertManagement.CreatorType.Student;
                        break;
                }
                foreach (JToken token in JsonArray)
                {
                    ReseivedMessage message = new ReseivedMessage(token.Value<string>("text"), type);
                    message.User = token.Value<string>("fromUserId");
                    message.time = DateTime.Parse(token.Value<string>("datetime"));
                    resievedMessages.Add(message);
                }
            }
            else if (!(backend.code == System.Net.HttpStatusCode.Conflict) && !(backend.code == System.Net.HttpStatusCode.NotFound))
            {
                throw new Exception(backend.code.ToString());
            }
        }
    }
}

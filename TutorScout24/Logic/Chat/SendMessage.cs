﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using TutorScout24.Logic.UserManagement;
using TutorScout24.Logic.AdvertManagement;
using Windows.UI;
using Windows.UI.Xaml;

namespace TutorScout24.Logic.Chat
{
    class SendMessage : IMessage
    {
        public SendMessage(string text)
        {
            this.text = text;
            time = DateTime.Now;
            switch (CurrentUser.type)
            {
                case CreatorType.Student:
                    color = new SolidColorBrush(Color.FromArgb(150, 219, 46, 0));
                    break;
                case CreatorType.Tutor:
                    color = new SolidColorBrush(Color.FromArgb(150, 0, 50, 233));
                    break;
            }
        }
        public string text { get; }

        public DateTime time { set;  get; }

        public SolidColorBrush color { get; }

        public HorizontalAlignment textAlignment { get { return HorizontalAlignment.Right; }}

        public string User { get; set; }
        public bool notread { get { return false; } set { } }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.AdvertManagement;
using TutorScout24.Logic.UserManagement;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace TutorScout24.Logic.Chat
{
    class ReseivedMessage : IMessage
    {
        public ReseivedMessage(string text, CreatorType type)
        {
            this.text = text;
            time = DateTime.Now;
            switch (type)
            {
                case CreatorType.Student:
                    color = new SolidColorBrush(Color.FromArgb(255, 219, 46, 0));
                    break;
                case CreatorType.Tutor:
                    color = new SolidColorBrush(Color.FromArgb(255, 0, 50, 233));
                    break;
            }
        }
        public string text { get; }

        public DateTime time { set;  get; }

        public SolidColorBrush color { get; }

        public HorizontalAlignment textAlignment { get { return HorizontalAlignment.Left; } }

        public string User { set; get; }
        public bool notread { get; set; }
    }
}

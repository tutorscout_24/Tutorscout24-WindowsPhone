﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace TutorScout24.Logic.Chat
{
    interface IMessage
    {
        string text { get; }
        DateTime time { get; }
        SolidColorBrush color { get; }
        HorizontalAlignment textAlignment { get; }
        string User { set; get; }
        bool notread { set; get; }
    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.UserManagement;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;

namespace TutorScout24.Logic.Chat
{
    class Chat
    {
        public string userName;
        public DateTime time;
        public string lastMessageText;
        public string lastMessageIcon { get { if (lastMessageSend) { return "\uE111" ; } return "\uE112" ; } }
        public bool lastMessageSend;
        public int unreadMessages;

    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TutorScout24.Logic.UserManagement;

namespace TutorScout24.Logic.Chat
{
    class User
    {
        public User(string userID)
        {
            Backend backend = new Backend();
            dynamic userRequestJson = new JObject();
            userRequestJson.userToFind = userID;
            userRequestJson.authentication = CurrentUser.getAuthenticationJson();

            if(AsyncHelpers.RunSync<bool>(backend.Post("/user/userInfo", userRequestJson.toString)))
            {
                this.userID = userID;
                JObject userJson = JObject.Parse(backend.returnContent);
                firstName = userJson["firstName"].Value<string>();
                lastName = userJson["lastName"].Value<string>();
                age = userJson["age"].Value<string>();
                gender = userJson["gender"].Value<string>();
                description = userJson["description"].Value<string>();
                maxGraduation = userJson["maxGraduation"].Value<string>();
            }
            else
            {
                switch (backend.code)
                {
                    case System.Net.HttpStatusCode.BadRequest:
                        throw new Exception("Es gibt keinen Benutzer mit dem Benutzernamen " + userID);
                    case System.Net.HttpStatusCode.Forbidden:
                        throw new Exception("Sie können diesen Benutzer nicht ansehen.");
                    case System.Net.HttpStatusCode.InternalServerError:
                        throw new Exception("Server Error");
                }
            }
        }
        public string userID;
        public string firstName;
        public string lastName;
        public string age;
        public string gender;
        public string description;
        public string maxGraduation;
    }
}

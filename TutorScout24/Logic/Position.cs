﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace TutorScout24.Logic
{
    class Position
    {
        public Geoposition geoposition { set; get; }
        public Position()
        {
            savePosition();
        }

        public async void savePosition()
        {
            geoposition = null;
            var accessStatus = await Geolocator.RequestAccessAsync();
            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    Geolocator geolocator = new Geolocator();
                    geoposition = await geolocator.GetGeopositionAsync();
                    break;
                case GeolocationAccessStatus.Denied:
                    //throw new Exception("access denied");
                    break;
                case GeolocationAccessStatus.Unspecified:
                   // throw new Exception("Unspecified");
                   break;
            }
        }

    }
}
